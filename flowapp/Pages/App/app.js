import React from 'react';
import { Link } from 'react-router'
import {Button} from 'react-bootstrap'

class App extends React.Component{
    render(){
        return (
            <div>
                <h1>Hi</h1>
                <Button>click</Button>
                <ul role="nav">
                <li><Link to="/task">go to task</Link></li>
                </ul>
            </div>
        )
    }
}

export default App;
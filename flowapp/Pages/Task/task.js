import React from 'react';
import { Link } from 'react-router'

class Task extends React.Component{
    render(){
        return (
            <div>
            <h1>Task</h1>
            <ul role="nav">
                <li><Link to="/">go home</Link></li>
            </ul>
        </div>
        )
    }
}

export default Task;

//need to remove the below since it is not used anymore
/*export const setVisibilityFilter = (filter)=>(
{ //Object Expression
        type: 'SET_VISIBILITY_FILTER',
        filter: filter

})*/

export const addTodo =(text)=>({
        type: 'ADD_TODO',
        id: v4(), // use uuid instead of nextTodoId++
        text
    })
/*
//Action Creator
export function addTodo(text){
    return {
        type: 'ADD_TODO',
        id: v4(), // use uuid instead of nextTodoId++
        text
    }
}*/

//Action Creators
export const toggleTodo = (id)=>({
    type: 'TOGGLE_TODO',
    id
})

import {loadState,saveState} from './localStorage';
import todoApp from './reducers';
import throttle from 'lodash/throttle'
import {createStore} from 'redux'


const configureStore =()=> {
    const persistedState = loadState(); // load Saved State from localStorage

    const store = createStore(todoApp, persistedState); // createstore take the reducers as input + overwrite with default values

    store.subscribe(throttle(()=> { //saving state when store changes + throttle it so that it does it every few seconds and not always
        saveState({todos: store.getState().todos})
    }, 1000))

    return store;
}

export default configureStore
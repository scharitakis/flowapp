
//NOT USED ANYMORE
import {combineReducers} from 'redux'



//this is a todo reducer
const todos = (state=[],action) =>{
    switch(action.type){
        case 'ADD_TODO':
            return [
                ...state,
                todo(state,action)
            ];
        case 'TOGGLE_TODO':{
            return state.map(t=>todo(t,action))
        }
        default:
            return state;
    }

}

//Composition is actually splitting the functionality
const todo = (state,action)=>{
    switch(action.type){
        case 'ADD_TODO':
            return {
                id:action.id,
                text:action.text,
                completed:false
            }
        case 'TOGGLE_TODO':
            if(state.id!== action.id){ // if not the todo that we want return the todo
                return state;
            }
            //else return the a new todo with the completed property changed
            return {
                ...state,
                completed: !state.completed
            }

    }
}


//store as an object
const visibilityFilter = (state='SHOW_ALL',action)=>{
    switch(action.type){
        case 'SET_VISIBILITY_FILTER':
            return action.filter;
        default:
            return state;
    }
}

//conbine Reducers
const todoApp = combineReducers({
    todos:todos,
    visibilityFilter:visibilityFilter
})


export default todoApp
import FilterLink from './FilterLink'

//FUNCTIONAL COMPONENT
const Footer = () =>(
    <p>
        Show:
        {' '}
        <FilterLink
            filter='all'
        >ALL</FilterLink>
        {' '}
        <FilterLink
            filter='active'
        >ACTIVE</FilterLink>
        {' '}
        <FilterLink
            filter='completed'
        >COMPLETED</FilterLink>
    </p>
)

export default Footer;
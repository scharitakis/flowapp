import {connect} from 'react-redux'
import TodoList from './TodoList'
import {toggleTodo} from '../actions'
import {withRouter} from 'react-router'



import {getVisibleTodos} from '../reducers'

// (state,ownProps.params.filter) =
const mapStateToTodoListProps =(state,{params})=>({
    todos:getVisibleTodos(
        state,//state.todos,
        //state.visibilityFilter// Change this to get from the route
        params.filter || 'all'
    )
})

/* // no need check connect
const mapDispatchToTodoListProps = (dispatch)=>({
    onTodoClick(id){
        dispatch(toggleTodo(id)) //usage of action creator found in actions
    }
})
*/

// We use the below instead of using a component with the didmount that subscribes to store
//withRouter passes any params from Routes (3.0 react-router)
const VisibleTodoList = withRouter(connect(
    mapStateToTodoListProps,
    {onTodoClick:toggleTodo}//mapDispatchToTodoListProps // another way of mapDispatchToTodoListProps
)(TodoList));

export default VisibleTodoList
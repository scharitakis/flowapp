import {connect} from 'react-redux'
import {v4} from 'node-uuid'
import {addTodo} from '../actions'


let AddTodo = ({dispatch})=>{ //PS see that this is a let(variable) and not a const
    let input;
    return (
        <div>
            <input ref={node=>{
                input = node;
            }} />
            <button onClick={()=>{
                dispatch(addTodo(input.value))
                input.value='';
            }
            } >Add todo</button>
        </div>
    )
};

AddTodo = connect()(AddTodo)

export default AddTodo;
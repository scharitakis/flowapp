import React from 'react'
import AddTodo from './AddTodo'
import VisibleTodoList from './VisibleTodoList'
import Footer from './Footer'

// the params is available from the router
// Thus we pass the filter from the
// Better way of passing the params from the router is by using the withRouter from the react-router
//Thus we remove the App =({params})=>( and the filter check VisibletodoList
const App =()=>(
    <div>
        <AddTodo />
        <VisibleTodoList
         //filter={params.filter||'all'} // we do not want to pass it using the params
        />
        <Footer />
    </div>
);

export default App
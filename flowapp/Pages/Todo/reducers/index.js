
// we want to get getVisibleTodos from reduce but has the same name
import todos, * as fromTodos from './todos'
//import visibilityFilter from './visibilityFilter' //no need we have the router
import {combineReducers} from 'redux'

const todoApp = combineReducers({
    todos:todos,
    //visibilityFilter:visibilityFilter
})

export default todoApp;

//named export
export const getVisibleTodos =(state,filter)=>
    fromTodos.getVisibleTodos(state.todos,filter)
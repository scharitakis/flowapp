
//this is a todo reducer


//Composition is actually splitting the functionality
const todo = (state,action)=>{
    switch(action.type){
        case 'ADD_TODO':
            return {
                id:action.id,
                text:action.text,
                completed:false
            }
        case 'TOGGLE_TODO':
            if(state.id!== action.id){ // if not the todo that we want return the todo
                return state;
            }
            //else return the a new todo with the completed property changed
            return {
                ...state,
                completed: !state.completed
            }

    }
}

const todos = (state=[],action) =>{
    switch(action.type){
        case 'ADD_TODO':
            return [
                ...state,
                todo(undefined,action)
            ];
        case 'TOGGLE_TODO':{
            return state.map(t=>todo(t,action))
        }
        default:
            return state;
    }

}

export default todos;

export const getVisibleTodos = (todos,filter)=>{  //named export ()
    switch (filter){
        case 'all':
            return todos;
        case 'completed':
            return todos.filter(t=>t.completed)
        case 'active':
            return todos.filter(t=>!t.completed)
        default:
            throw new Error('Unknown filter')
    }
}
import React from 'react';
import ReactDom from 'react-dom';

//import App from './Pages/App/app'
//import Task from './Pages/Task/task'
import { createStore,combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'



//import { App, Home, Foo, Bar } from './components'
import App from "./Pages/app"
import Bar from "./Pages/Bar/bar"
import Foo from "./Pages/Foo/foo"
import Home from "./Pages/Home/home"


const preloadedState = window._PRELOADSTATE
import {configureStore,DevTools} from './serverStoreConf';
const store = configureStore(browserHistory, preloadedState);
import { syncHistoryWithStore} from 'react-router-redux'
const history = syncHistoryWithStore(browserHistory, store)


ReactDom.render((
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={Home}/>
                <Route path="foo" component={Foo}/>
                <Route path="bar" component={Bar}/>
            </Route>
        </Router>
    </Provider>
), document.getElementById('app'))

ReactDom.render(
    <Provider store={store}>
        <DevTools/>
    </Provider>,
    document.getElementById('devtools')
)
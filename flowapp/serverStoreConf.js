import React from 'react'
import { createStore, combineReducers, compose, applyMiddleware } from 'redux'

import { routerReducer, routerMiddleware } from 'react-router-redux'
import * as reducers from './Reducers'

import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

var DevTools = createDevTools(
    <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
        <LogMonitor theme="tomorrow" preserveScrollTop={false} />
    </DockMonitor>
)

function configureStore(history, initialState) {
    try {
        const reducer = combineReducers({
            ...reducers,
            routing: routerReducer
        })

        let devTools = []
        if (typeof document !== 'undefined') {
            devTools = [ DevTools.instrument() ]
        }

        var store = createStore(
            reducer,
            initialState,
            compose(
                applyMiddleware(
                    routerMiddleware(history)
                ),
                ...devTools
            )
        )

    }catch(e){
        console.log('configure Store Error',e)
    }

    return store
}


module.exports = {configureStore:configureStore,
    DevTools:DevTools
}
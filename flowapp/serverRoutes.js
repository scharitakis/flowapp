import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import App from "./Pages/app"
import Bar from "./Pages/Bar/bar"
import Foo from "./Pages/Foo/foo"
import Home from "./Pages/Home/home"

var routes = (
            <Route path="/" component={App}>
                <IndexRoute component={Home}/>
                <Route path="foo" component={Foo}/>
                <Route path="bar" component={Bar}/>
            </Route>
);

module.exports=routes;
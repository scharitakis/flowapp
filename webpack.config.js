


module.exports = {
    entry: './flowapp/clientRoutes.js',

    output: {
        filename: 'bundle.js',
        path : './public/flowappAssets/'
    },

    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react' }
        ]
    }
}
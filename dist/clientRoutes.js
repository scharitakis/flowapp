'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _redux = require('redux');

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _app = require('./Pages/app');

var _app2 = _interopRequireDefault(_app);

var _bar = require('./Pages/Bar/bar');

var _bar2 = _interopRequireDefault(_bar);

var _foo = require('./Pages/Foo/foo');

var _foo2 = _interopRequireDefault(_foo);

var _home = require('./Pages/Home/home');

var _home2 = _interopRequireDefault(_home);

var _serverStoreConf = require('./serverStoreConf');

var _reactRouterRedux = require('react-router-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*import * as reducers from './Reducers'
const reducer = combineReducers({
    ...reducers,
    routing: routerReducer
})



const store = createStore(
    reducer,
    //DevTools.instrument()
)*/

//import { App, Home, Foo, Bar } from './components'
var preloadedState = window._PRELOADSTATE;

//import App from './Pages/App/app'
//import Task from './Pages/Task/task'

var store = (0, _serverStoreConf.configureStore)(_reactRouter.browserHistory, preloadedState);

var history = (0, _reactRouterRedux.syncHistoryWithStore)(_reactRouter.browserHistory, store);

_reactDom2.default.render(_react2.default.createElement(
    _reactRedux.Provider,
    { store: store },
    _react2.default.createElement(
        _reactRouter.Router,
        { history: history },
        _react2.default.createElement(
            _reactRouter.Route,
            { path: '/', component: _app2.default },
            _react2.default.createElement(_reactRouter.IndexRoute, { component: _home2.default }),
            _react2.default.createElement(_reactRouter.Route, { path: 'foo', component: _foo2.default }),
            _react2.default.createElement(_reactRouter.Route, { path: 'bar', component: _bar2.default })
        )
    )
), document.getElementById('app'));

_reactDom2.default.render(_react2.default.createElement(
    _reactRedux.Provider,
    { store: store },
    _react2.default.createElement(_serverStoreConf.DevTools, null)
), document.getElementById('devtools'));
'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _app = require('./Pages/app');

var _app2 = _interopRequireDefault(_app);

var _bar = require('./Pages/Bar/bar');

var _bar2 = _interopRequireDefault(_bar);

var _foo = require('./Pages/Foo/foo');

var _foo2 = _interopRequireDefault(_foo);

var _home = require('./Pages/Home/home');

var _home2 = _interopRequireDefault(_home);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routes = _react2.default.createElement(
    _reactRouter.Route,
    { path: '/', component: _app2.default },
    _react2.default.createElement(_reactRouter.IndexRoute, { component: _home2.default }),
    _react2.default.createElement(_reactRouter.Route, { path: 'foo', component: _foo2.default }),
    _react2.default.createElement(_reactRouter.Route, { path: 'bar', component: _bar2.default })
);

module.exports = routes;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.count = undefined;

var _count2 = require('./count');

var _count3 = _interopRequireDefault(_count2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.count = _count3.default;
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = update;

var _constants = require('../constants');

var initialState = {
    number: 1
};

function update() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments[1];

    if (action.type === _constants.INCREASE) {
        return { number: state.number + action.amount };
    } else if (action.type === _constants.DECREASE) {
        return { number: state.number - action.amount };
    }
    return state;
}
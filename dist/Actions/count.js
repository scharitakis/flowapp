'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.increase = increase;
exports.decrease = decrease;

var _constants = require('../constants');

function increase(n) {
    return {
        type: _constants.INCREASE,
        amount: n
    };
}

function decrease(n) {
    return {
        type: _constants.DECREASE,
        amount: n
    };
}
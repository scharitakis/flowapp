'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Bar;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Bar() {
    return _react2.default.createElement(
        'div',
        null,
        'And I am Bar!'
    );
}
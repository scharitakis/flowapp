'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = App;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function App(_ref) {
    var children = _ref.children;

    return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
            'header',
            null,
            'Links:',
            ' ',
            _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' },
                'Home'
            ),
            ' ',
            _react2.default.createElement(
                _reactRouter.Link,
                { to: '/foo' },
                'Foo'
            ),
            ' ',
            _react2.default.createElement(
                _reactRouter.Link,
                { to: '/bar' },
                'Bar'
            )
        ),
        _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
                'button',
                { onClick: function onClick() {
                        return _reactRouter.browserHistory.push('/foo');
                    } },
                'Go to /foo'
            )
        ),
        _react2.default.createElement(
            'div',
            { style: { marginTop: '1.5em' } },
            children
        )
    );
}
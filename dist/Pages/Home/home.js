'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _count = require('../../Actions/count');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Home(_ref) {
    var number = _ref.number,
        increase = _ref.increase,
        decrease = _ref.decrease;

    return _react2.default.createElement(
        'div',
        null,
        'Some state changes:',
        number,
        _react2.default.createElement(
            'button',
            { onClick: function onClick() {
                    return increase(1);
                } },
            'Increase'
        ),
        _react2.default.createElement(
            'button',
            { onClick: function onClick() {
                    return decrease(1);
                } },
            'Decrease'
        )
    );
}

exports.default = (0, _reactRedux.connect)(function (state) {
    return { number: state.count.number };
}, { increase: _count.increase, decrease: _count.decrease })(Home);
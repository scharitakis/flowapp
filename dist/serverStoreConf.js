'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _redux = require('redux');

var _reactRouterRedux = require('react-router-redux');

var _Reducers = require('./Reducers');

var reducers = _interopRequireWildcard(_Reducers);

var _reduxDevtools = require('redux-devtools');

var _reduxDevtoolsLogMonitor = require('redux-devtools-log-monitor');

var _reduxDevtoolsLogMonitor2 = _interopRequireDefault(_reduxDevtoolsLogMonitor);

var _reduxDevtoolsDockMonitor = require('redux-devtools-dock-monitor');

var _reduxDevtoolsDockMonitor2 = _interopRequireDefault(_reduxDevtoolsDockMonitor);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var DevTools = (0, _reduxDevtools.createDevTools)(_react2.default.createElement(
    _reduxDevtoolsDockMonitor2.default,
    { toggleVisibilityKey: 'ctrl-h', changePositionKey: 'ctrl-q' },
    _react2.default.createElement(_reduxDevtoolsLogMonitor2.default, { theme: 'tomorrow', preserveScrollTop: false })
));

function configureStore(history, initialState) {
    try {
        var reducer = (0, _redux.combineReducers)(_extends({}, reducers, {
            routing: _reactRouterRedux.routerReducer
        }));

        var devTools = [];
        if (typeof document !== 'undefined') {
            devTools = [DevTools.instrument()];
        }

        var store = (0, _redux.createStore)(reducer, initialState, _redux.compose.apply(undefined, [(0, _redux.applyMiddleware)((0, _reactRouterRedux.routerMiddleware)(history))].concat(_toConsumableArray(devTools))));
    } catch (e) {
        console.log('configure Store Error', e);
    }

    return store;
}

module.exports = { configureStore: configureStore,
    DevTools: DevTools
};
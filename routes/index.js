var express = require('express');
var router = express.Router();
var ReactRouter = require('react-router');
var ReactDOMServer = require('react-dom/server');
var React = require('react')

var match = ReactRouter.match;
var RouterContext  = ReactRouter.RouterContext;
var createMemoryHistory = ReactRouter.createMemoryHistory;

var Provider =require('react-redux').Provider;

var routes = require('../dist/serverRoutes')


var ReacRouterRedux = require('react-router-redux')
var syncHistoryWithStore =ReacRouterRedux.syncHistoryWithStore

var configureStore = require('../dist/serverStoreConf').configureStore;

/* GET home page. */
router.get('*', function(req, res, next) {
  try {
    var preloaded_store ={count:{number: 5}}

    var memoryHistory = createMemoryHistory(req.url)
    var store = configureStore(memoryHistory,preloaded_store)
    var history = syncHistoryWithStore(memoryHistory, store)

    match({history: history, routes: routes, location: req.url},function(error, redirectLocation, renderProps){
      if (error) {
        console.log(error)
        res.status(500).send(error.message)
      } else if (redirectLocation) {
        res.redirect(302, redirectLocation.pathname + redirectLocation.search)
      } else if (renderProps) {
        // You can also check renderProps.components or renderProps.routes for
        // your "not found" component or route respectively, and send a 404 as
        // below, if you're using a catch-all route.
        //res.status(200).send(renderToString(<RouterContext {...renderProps} />))
        try {
          var rString = ReactDOMServer.renderToString(
              <Provider store={store}>
                <RouterContext {...renderProps} />
              </Provider>
          )
          res.render('index', {
            title: 'Express',
            markup: rString,
            preloadState:JSON.stringify(preloaded_store), // Pass the store configured in the backend
          });
        } catch (e) {
          res.status(500).send(e)
        }
      } else {
        res.status(404).send('Not found')
      }
    })
  }catch(e){
    console.log('asdasdadadad')
    console.log(e)
  }

});

module.exports = router;
